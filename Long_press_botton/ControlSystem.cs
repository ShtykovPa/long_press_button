using System;
using Crestron.SimplSharp;                          	// For Basic SIMPL# Classes
using Crestron.SimplSharpPro;                       	// For Basic SIMPL#Pro classes
using Crestron.SimplSharpPro.CrestronThread;        	// For Threading
using Crestron.SimplSharpPro.Diagnostics;		    	// For System Monitor Access
using Crestron.SimplSharpPro.DeviceSupport;         	// For Generic Device Support
using Crestron.SimplSharpPro.UI;

namespace Long_press_test
{
	public class ControlSystem : CrestronControlSystem
	{
		private Tsw760 TouchPanel;
		private Stopwatch PressingTimer = new Stopwatch();


		public ControlSystem()
			: base()
		{
			try
			{
				Thread.MaxNumberOfUserThreads = 20;

				TouchPanel = new Tsw760(0x10, this);
				TouchPanel.SigChange += new SigEventHandler(TouchPanel_SigChange);
				TouchPanel.Register();

			}
			catch (Exception e)
			{
				ErrorLog.Error("Error in the constructor: {0}", e.Message);
			}
		}

		void TouchPanel_SigChange(BasicTriList currentDevice, SigEventArgs args)
		{
			switch (args.Sig.Type)
			{
				case eSigType.Bool:

					switch (args.Sig.Number)
					{
						case 100:
							if (args.Sig.BoolValue == true)
							{
								PressingTimer.Start();
							}
							else
							{
								PressingTimer.Stop();
								if (PressingTimer.ElapsedMilliseconds <= 400)
								{
									CrestronConsole.PrintLine("Button {0} fast tap", args.Sig.Number);
								}
								else if (PressingTimer.ElapsedMilliseconds >= 1500)
								{
									CrestronConsole.PrintLine("Button {0} long press", args.Sig.Number);
								}
								PressingTimer.Reset();
							}
							break;
					}
					break;

				case eSigType.UShort:

					break;

				case eSigType.String:

					break;
			}
		}
	}
}